Jira Connector OAuth Demo
==================================

## Introduction
This demo explains how to configure and use this connector with OAuth2 authentication enabled on Jira instance. 

## Prerequisites

* Java 8  
* Anypoint Studio 7.x  
* Mule Runtime 4.2.2 and higher  
* Jira instance access  
* Jira connector v1.0.0  
* Registered OAuth protected application in Jira Application Management  

## Create OAuth application in Jira
The first step is to create an OAuth2 (3LO) enabled application in Jira App Management, get the clientId and clientSecret and set the callback URL.
For complete guide on how to set up Jira OAuth application please visit https://developer.atlassian.com/cloud/jira/platform/oauth-2-authorization-code-grants-3lo-for-apps/.
After the app is created save your clientId and clientSecret which are available in the app details.

![Jira Client Credentials](images/jira-client-credentials.png)

Then navigate to section APIS and FEATURES -> OAuth 2.0 (3LO) and set your Callback URL to `https://localhost:8081/callback`

![Jira Client Credentials](images/jira-callback.png)

## Import the project

* Go to **File > Import**.
* Select **Anypoint Studio Project from File System** (under the parent folder "Anypoint Studio").
* Provide the root **path to the demo (jira-oauth-demo)** project folder.
* Select **Mule Runtime (4.2.2 EE and higher)** .
* Click **Finish**.
* Set the saved credentials inside the file `src/main/resources/mule-app.properties`.

```
jira.oauth.clientId=
jira.oauth.clientSecret=
```

Explanation of credentials:


#### Field | Description

**|Audience|** Base url of the Atlassian API - Enter `'api.atlassian.com'` as provided in the example.  
**|Prompt|** If user has to be presented by prompt screen, enter `'consent'` as provided in the example.  
**|Consumer Key|** Enter the client ID from the created OAuth application.  
**|Consumer Secret|** Enter the client secret from the created OAuth application.  
**|Authorization url|** Enter the service provider authorization url.  
**|Access token url|** Enter the service provider access token url.  
**|Scopes|** The OAuth scopes to be requested during the dance, if not provided it will default to those in the annotation. Scope offline_access should be always present as it ensures the returning the refresh token. To get more information about scope values, please visit https://developer.atlassian.com/cloud/jira/platform/scopes/.  
**|Listener Config|** Enter the name of HTTP config used in the Mule application.  
**|Authorize Path|** Enter the path of the local http endpoint which triggers the OAuth dance.  
**|Callback path|** Enter the path of accessToken callback endpoint.  


* Go to `src/main/mule` and open **jira-oauth-demo.xml**. Navigate to Global Elements tab and locate Jira Rest Configuration named `Jira_OAuth_Config`
![Jira Global Elements](images/jira-global-elements.png)

* Open the **Global Element Configuration** and make sure that registered application's **(OAuth Application -> APIS and FEATURES -> OAuth 2.0 (3LO))** Callback URL matches _External callback url_ parameter in this configuration.
If there is a difference, then update the registered application's _Redirect URL_ based on the deployment environment. Jira App Management requires using the https protocol.
  * CloudHub deployment - https://{cloudhub-instance}/callback 
  * Local deployment (Anypoint studio) - https://localhost:{port}/callback

  The following images showcase example configuration for local deployment.
  
![OAuth Configuration](images/jira-oauth-config.png)

## Run the demo

Go to **Run > Run As > Mule Application** and wait until the application deploys.

### Initiate OAuth dance
Run in browser `https://localhost:8081/authorize?resourceOwnerId=demo` to start the OAuth dance.

NOTE: If _Resource owner id_ parameter is not set in OAuth configuration then simply use the following link `https://localhost:8081/authorize` to initiate the OAuth dance

Jira will redirect to the login screen:
![Login Screen](images/jira-login-screen.png)

Here you will need to enter login details used to access the Jira instance and click **Continue** button. 
After that you will be redirected to the following screen:
![Authorize Screen](images/jira-authorize-screen.png)

On this screen choose the authorization site and click **Accept** button. You will be redirected to the next screen which confirms that OAuth dance was successful and the access token was retrieved:  
![Access Token](images/jira-access-token.png)

### Perform Get Incidents operation
Run in browser `https://localhost:8081/getIssueTypesOAuth` to perform the Get All Issue Types operation. Yout should receive successful response:
![Successful Response](images/jira-response.png)

## See more
* For additional technical information on the Jira Connector, visit the [technical reference documentation](https://hotovo.gitlab.io/mule-connectors/mule4-jira-rest-connector/docs/jira-rest-connector-reference.html).
* For more information on the JiraRest API, go to the [Jira API documentation page](https://https://developer.atlassian.com/cloud/jira/platform/rest/v3/).