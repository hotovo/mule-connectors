Jira Connector Demo
==================================
## Introduction
This demo application shows the set of basic CRUD operations which are available by the Jira connector. These operations allow you to create, update, get and delete the Jira issues.

## Prerequisites
* Java 8
* Anypoint Studio 7.x
* Mule Runtime  4.1.1 and later
* Jira access credentials.

## Import the project
* Navigate to **File > Import**
* Select **Anypoint Studio Project from File System** (under the parent folder “Anypoint Studio”)
* Provide the root path to the demo project folder (demo/jira-crud-demo).
* Select Mule Runtime
* Click **Finish**.
* Fill Jira credentials inside the properties file `src/main/resources/mule-artifact.properties`. Fill out the following details:

```
config.username=
config.apiToken=
config.serverUrl=
```
	
**To create the Jira API token please follow the action steps described here :**
     https://confluence.atlassian.com/cloud/api-tokens-938839638.html

* Open the **Global Elements** tab and click on **Create**.
* Find and select the **Jira Rest Configauration**
* From the Connection section choose the **Basic Authentication**.
* Fill out your Jira server URL, your username and your created API token.

![Demo JIRA_Configuration](images/global_config.png)

* Click the **Test Connection** button to ensure there is connectivity with the Jira instance. A successful message should pop-up.

* Open a browser and access the URL **http://localhost:8081**. You should see the demo application deployed:

## Run the demo

### Create Issue

![Demo Create_Issue](images/create_issue.png)

* Ensure **Summary**, **Description**, **IssueType ID**, **IssuePriority ID**, **Project ID** are filled.
* Click the button **Create** and wait a few moments to finish processing.
* You should see the alert box with success message and the created Issue key . 

### Get Issue	

![Demo Get_Issue](images/get_issue.png)

* Field **Issue Key** is automatically filled.
* Click the button **Read** and wait a few moments to finish processing.
* You should see the alert box with success message and **Result** text field with the Issue object. 

###  Update Issue

![Demo Update_Issue](images/update_issue.png)

* Field **Issue Key** is again automatically filled.
* Ensure **Updated Description** field is filled.
* Click the button **Update** and wait a few moments to finish processing.
* You should see the alert box with success message.

### Delete Issue

*![Demo Delete_Issue](images/delete_issue.png)

* Field **Issue Key** is automatically filled.
* Click the button **Delete** and wait a few moments to finish processing.
* You should see the alert box with success message.

## See more
* For additional technical information on the Jira Connector, visit the [technical reference documentation](https://hotovo.gitlab.io/mule-connectors/mule4-jira-rest-connector/docs/jira-rest-connector-reference.html).
* For more information on the JiraRest API, go to the [Jira API documentation page](https://https://developer.atlassian.com/cloud/jira/platform/rest/v3/).