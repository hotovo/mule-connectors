# JIRA Rest Connector Issue CRUD example

+ [Use Case](#usecase)
+ [Run it!](#runit)
	* [Running on Studio](#runonstudio)
	* [Properties to be configured](#propertiestobeconfigured)
+ [Customize It!](#customizeit)
+ [Report an Issue](#reportissue)


### Author
Hotovo s.r.o.

### Supported Mule runtime versions
Mule 3.6.x or higher

### JIRA supported versions
JIRA 6.x or higher

# Use Case 
This example application shows simplified use of the JIRA Rest Connector. It serves as a simple example about how to create, read, update and delete issues based on user input into HTML form. 
The section immediately below describes the functions of each of the flows in the application. 

This example assumes that you are familiar with Mule, the Anypoint Studio interface, Global Elements, DataSense, and the DataWeave transformer. Further, it is assumed that you are 
familiar with JIRA and have a JIRA account. Learn more about flows and subflows to gain more insight into the design of this example.

# Run it! 
Follow these simple steps to get this example running

### Where to Download Anypoint Studio and Mule ESB
First thing to know if you are a newcomer to Mule is where to get the tools.

+ You can download Anypoint Studio from this [Location](http://www.mulesoft.com/platform/mule-studio)
+ You can download Mule ESB from this [Location](http://www.mulesoft.com/platform/soa/mule-esb-open-source-esb)

### Installing connector
In order to continue, you need to have Jira Rest connector installed in your Anypoint Studio. To do so please check Jira Rest connector [user guide](https://hotovo.gitlab.io/mule-connectors/jira-rest-connector/docs/user-manual.html "user guide"). 

### Importing example into Studio
Mule Studio offers several ways to import a project into the workspace, for instance: 

+ Anypoint Studio generated Deployable Archive (.zip)
+ Anypoint Studio Project from External Location
+ Maven-based Mule Project from pom.xml
+ Mule ESB Configuration XML from External Location

You can find a detailed description on how to do so in this [Documentation Page](http://www.mulesoft.org/documentation/display/current/Importing+and+Exporting+in+Studio).

### Running on Studio 
Once you have imported this example into Anypoint Studio you need to follow these steps to run it:

+ Install Jira Rest Connector into Anypoint Studio 
+ Locate the properties file `mule-app.properties`, in src/main/app/ folder
+ Complete all the required properties in file as per example:  

### 
    jirarest.username=jirauser
    jirarest.password=passwd
    jirarest.host=https://example.atlassian.net

+ Once that is done, right click on project folder 
+ Hover you mouse over `"Run as"`
+ Click on  `"Mule Application"`
+ Open your browser at [localhost:8081](localhost:8081)

### Play with it!
Inside your browser, you can either follow default create -> get -> update -> delete pipeline or you can select any of these operations and try it. If you insert invalid values, application will return error popup. Please refer to following image.

![Demo Application](readme.png)


# Customize It!
This section contains a brief description about how this example works. Flow configuration file is separated in five disctinct flows to make it more readable. 

### html-form-flow

This flow use Parse Template component to render HTML page containing form used to input user data regarding issue. 

### create-issue-flow

Dataweave transformer in this flow transform input from html form to payload used by JiraRest connector to create new issue. Result of the operation is logged to console.

### read-issue-flow

This flow retrieve issue based on key obtained as a result of previeous flow. Retrieved issue is then logged to console.

### update-issue-flow

Dataweave transformer in this flow transform user input from html form to payload used by JiraRest connector to update description of existing issue indetified by key. This key is then passed to delete flow.

### delete-issue-flow

This flow will remove issue based on key from previous flow. As a result, alert dialog is shown.

# Report an Issue
We use JIRA for tracking issues with this connector. You can report new issues at this location https://hotovo.jira.com
