= JIRA Rest Connector Release Notes

== Jira Rest Connector 1.3.0 - 19.3.2018

Release Notes for version V1.3.0 of the JIRA Rest Connector. These Release Notes accompany the http://www.mulesoft.org/documentation/display/current/JIRA+Rest+Connector guide.
 
=== Version 1.3.0 - Compatibility
The JIRA Rest connector is compatible with:

|===
|Application/Service|Version

|Mule Runtime|3.6.x EE
|JIRA|6.x
|===

=== Version 1.3.0 - Features
* New configuration strategy - OAuth 1.0a&nbsp;  

* HTTP proxy support

=== Version 1.3.0 - Added in this Release
* New configuration strategy - OAuth 1.0a&nbsp;  

* HTTP proxy support  

* User update operation was removed because it's no longer supported by Jira API. This is related to new Atlassian Account / SAML functionality and you can find out more link:https://confluence.atlassian.com/jirakb/user-management-rest-api-changes-in-jira-cloud-858756548.html[here].

=== Version 1.3.0 - Fixed in this Release
* N/A

=== Version 1.3.0 - Known Issues
* N/A

=== Support Resources

* JIRA Rest Connector examples can be found at https://gitlab.com/hotovo/mule-connectors/tree/master/jira-rest-connector/

* Learn how to link:/mule-user-guide/v/3.8/installing-connectors[Install Anypoint Connectors] using Anypoint Exchange.
* Access the link:http://forum.mulesoft.org/mulesoft[MuleSoft Forum] to pose questions and get help from Mule’s broad community of users.
* To access MuleSoft’s expert support team, link:http://www.mulesoft.com/mule-esb-subscription[subscribe] and log in to MuleSoft’s link:http://www.mulesoft.com/support-login[Customer Portal].



== Jira Rest Connector 1.2.0 - 19.5.2017

Release Notes for version V1.2.0 of the JIRA Rest Connector. These Release Notes accompany the http://www.mulesoft.org/documentation/display/current/JIRA+Rest+Connector guide.
 
=== Version 1.2.0 - Compatibility
The JIRA Rest connector is compatible with:

|===
|Application/Service|Version

|Mule Runtime|3.6.x EE
|JIRA|6.x
|===

=== Version 1.2.0 - Features
* N/A

=== Version 1.2.0 - Added in this Release
* Added support for Mule 3.8.x runtimes
* New License Management 

=== Version 1.2.0 - Fixed in this Release
* N/A

=== Version 1.2.0 - Known Issues
* N/A


== Jira Rest Connector 1.1.0 - 9.12.2015

=== Version 1.1.0 - Compatibility
The JIRA Rest connector is compatible with:

|===
|Application/Service|Version

|Mule Runtime|3.6.x, 3.7.x
|JIRA|6.x
|===

=== Version 1.1.0 - Features
* Operation Issues - bulk create

=== Version 1.1.0 - Added in this Release
* New operation - Issues - bulk create

=== Version 1.1.0 - Fixed in this Release
* N/A

=== Version 1.1.0 - Known Issues
* N/A


== Jira Rest Connector 1.0.0 - 20.4.2015
 
=== Version 1.0.0 - Compatibility
The JIRA Rest connector is compatible with:

|===
|Application/Service|Version

|Mule Runtime|3.6.x, 3.7.x
|JIRA|6.x
|===

=== Version 1.0.0 - Features
* CRUD operations for Attachments
* CRUD operations for Comments
* CRUD operations for Issues
* CRUD operations for Issue Links
* CRUD operations for Issue Remote Links
* CRUD operations for Projects
* CRUD operations for Users
* CRUD operations for User Groups
* CRUD operations for Versions
* CRUD operations for Worklogs
* Myself
* Notify
* Search

=== Version 1.0.0 - Added in this Release
* N/A

=== Version 1.0.0 - Fixed in this Release
* N/A

=== Version 1.0.0 - Known Issues
* N/A
