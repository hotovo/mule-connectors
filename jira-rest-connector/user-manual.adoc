= JIRA Rest Connector
:keywords: Jira, rest
:imagesdir: _images
:toc: macro
:toclevels: 2
////
Image names follow "image:". Identify images using the connector name, preferably without the word "connector"
URLs should always follow "link:"
////

// Dropdown for Connector Version
//  Children: Technical Reference / Demos

toc::[]


== What is the Connector?

Integrate JIRA with existing SaaS and on-premises applications quickly and easily using the Atlassian JIRA Rest connector.
Using the connector, your application can perform several operations which JIRA  exposes via their REST API.

Read through this user guide to understand how to set up and configure a basic flow using the connector. Track feature additions, compatibility, limitations and API version updates with each release of the connector using the link:https://hotovo.gitlab.io/mule-connectors/jira-rest-connector/docs/release-notes.html[Connector Release Notes]. Review the connector operations and functionality using the link:https://hotovo.gitlab.io/mule-connectors/jira-rest-connector/javadoc/mule/jirarest-config.html[ Technical Reference] alongside the link:https://gitlab.com/hotovo/mule-connectors/tree/master/jira-rest-connector/jira-crud-demo[demo application].

MuleSoft maintains this connector under the link:https://docs.mulesoft.com/mule-user-guide/v/3.8/anypoint-connectors#connector-categories[MuleSoftCertified Category] support policy.


== Prerequisites

This document assumes that you are familiar with Mule,
link:https://docs.mulesoft.com/mule-user-guide/v/3.8/anypoint-connectors[Anypoint Connectors], and
link:https://docs.mulesoft.com/anypoint-studio/v/6[Anypoint Studio]. To increase your familiarity with Studio, consider completing a link:https://docs.mulesoft.com/anypoint-studio/v/6/basic-studio-tutorial[Anypoint Studio Tutorial]. This page requires some basic knowledge of link:https://docs.mulesoft.com/mule-user-guide/v/3.8/mule-concepts[Mule Concepts], link:https://docs.mulesoft.com/mule-user-guide/v/3.8/elements-in-a-mule-flow[Elements in a Mule Flow], and link:https://docs.mulesoft.com/mule-user-guide/v/3.8/global-elements[Global Elements].


[[requirements]]
=== Hardware and Software Requirements

For hardware and software requirements, please visit the link:https://docs.mulesoft.com/mule-user-guide/v/3.8/hardware-and-software-requirements[Hardware and Software Requirements] page.



=== Compatibility

|===
|Application/Service|Version

|Mule Runtime|3.6.x EE or higher
|JIRA|6.x or higher
|===

== How to Install

You can install the connector in Anypoint Studio using the instructions in
link:https://docs.mulesoft.com/getting-started/anypoint-exchange#installing-a-connector-from-anypoint-exchange[Installing a Connector from Anypoint Exchange].

=== Upgrading from an Older Version

If you’re currently using an older version of the connector, a small popup appears in the bottom right corner of Anypoint Studio with an "Updates Available" message.

. Click the popup and check for available updates. 
. Click the Connector version checkbox and click *Next* and follow the instructions provided by the user interface. 
. *Restart* Studio when prompted. 
. After restarting, when creating a flow and using the connector, if you have several versions of the connector installed, you may be asked which version you would like to use. Choose the version you would like to use.

Additionally, we recommend that you keep Studio up to date with its latest version.

== How to Configure

To use the JIRA Rest connector in your Mule application, you must configure a global JIRA Rest element that can be used by the  connector (read more about  link:https://docs.mulesoft.com/mule-user-guide/v/3.8/global-elements[Global Elements]). The JIRA Rest connector offers two global configuration options:

1. Basic Authentication
2. OAuth 1.0a

=== Basic Authentication
HTTP Basic Authentication configuration requires the following credentials: *Username*, *Password*, *ServerUrl*

[%header,cols="50a,50a"]
|===
|Field |Description
|*Username* |Enter the username to log into Jira.
|*Password* |Enter the corresponding password.
|*Server Url*|Enter the base URL of your Jira instance.
|===

image::jira-rest-global-element-props.png[<connector>-config]

=== OAuth 1.0a 

*Configure OAuth in Jira*

Before running the example, you need to configure your Jira instance to support OAuth 1.0a application link. To do so, follow https://developer.atlassian.com/cloud/jira/platform/jira-rest-api-oauth-authentication[these guidelines]. When prompted for Consumer Callback URL, enter __your-mule-app-base-url/callback__. This address must be accessible from outside, so if you are running locally, consider using tool like https://ngrok.com[Ngrok].

*Configuration in Anypoint Studio*

Oauth configuration in studio requires following attributes: *Jira server URL*, *Consumer Key* and *Pivate key*.

[%header,cols="50a,50a"]
|===
|Field |Description
|*Jira server URL* |Enter the base URL of your Jira instance.
|*Consumer Key* |Enter the consumer key you used in Jira Oauth configuration.
|*Pivate key*|Enter the private key from the key pair you used in Jira Oauth configuration.
|===

For example, please refer to following image.

image::jira-config-oauth1.PNG[oauth-config]

*Token Storage configuration*

Apart from required OAuth configuration, you can optionally configure where will connector store OAuth tokens. This configuration consist of fields shown in following table.

[%header,cols="50a,50a"]
|===
|Field |Description
|*Object store name* |Name of the object store for storing OAuth tokens.
|*Persistent* |Whether the object store should be persistent or not, default yes.
|*Token TTL* |Time To Live for stored tokens in milliseconds.
|*Max Tokens* |Specifies the maximum number of stored tokens.
|*Expiration Interval* |Specifies the expiration check interval in milliseconds.
|===

See following image for example of optional configuration.

image::jira-config-oauth2.PNG[tokens storage config]

[TIP]
Please note that when you configure any properties of *Expiration* group, you must either provide all **Token TTL**, **Max Tokens**, and **Expiration Interval**, or none of them.


=== Proxy Configuration

In enterprise environment, Jira instance can often be situated behind a proxy. If itś a HTTP proxy, you can configure the connector to reach it using *Proxy* tab in global configuration element. You can specify URI of proxy and also a username or password for authentication, as described in the next table.

[%header,cols="50a,50a"]
|===
|Field |Description
|*Proxy URI* |URI of a HTTP proxy the client connector should use. If the port component of the URI is absent then a default port of 8080 is assumed. If the property absent then no proxy will be utilized.
|*Proxy Username* |User name which will be used for HTTP proxy authentication. The property is ignored if no proxy URI has been set. If the property absent then no proxy authentication will be utilized.
|*Proxy Password* |User password which will be used for HTTP proxy authentication. The property is ignored if no proxy URI has been set. If the property absent then no proxy authentication will be utilized.
|===

For example refer to the following image.

image::jira-config-proxy.PNG[proxy config]

*HTTPS Proxy*
If you need to use HTTPS instead of HTTP proxy, you must set it up using system properties. For more info about HTTP proxies configuration, please visit http://docs.oracle.com/javase/7/docs/technotes/guides/net/proxies.html[Java Networking and Proxies Guide].


=== Required Connector Namespace and Schema

When designing your application in Studio, the act of dragging the connector from the palette into the Anypoint Studio canvas should automatically populate the XML code with the connector *namespace* and *schema location*.

*Namespace:* `http://www.mulesoft.org/schema/mule/jirarest`
*Schema Location:* `http://www.mulesoft.org/schema/mule/jirarest/current/mule-jirarest.xsd`

[TIP]
If you are manually coding the Mule application in Studio's XML editor or other text editor, define the namespace and schema location in the header of your *Configuration XML*, inside the `<mule>` tag.

[source, xml,linenums]
----
<mule xmlns="http://www.mulesoft.org/schema/mule/core"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:jirarest="http://www.mulesoft.org/schema/mule/jirarest"
      xsi:schemaLocation="
               http://www.mulesoft.org/schema/mule/core
               http://www.mulesoft.org/schema/mule/core/current/mule.xsd
               http://www.mulesoft.org/schema/mule/jirarest
               http://www.mulesoft.org/schema/mule/jirarest/current/mule-jirarest.xsd">

      <!-- put your global configuration elements and flows here -->

</mule>
----


=== Maven Dependency Information

Maven is backing the application, this XML snippet must be included in your `pom.xml` file.

[source,xml,linenums]
----
<dependency>
  <groupId>org.mule.modules</groupId>
  <artifactId>jirarest-connector</artifactId>
  <version>1.2.0</version>
</dependency>
----

[TIP]
====
Inside the `<version>` tags, put the desired version number, the word `RELEASE` for the latest release, or `SNAPSHOT` for the latest available version. The available versions to date are:

* *1.3.0*
* *1.2.0*
* *1.1.0*
* *1.0.0*
====


== Operations

* CRUD operations for Attachments
* CRUD operations for Comments
* CRUD operations for Issues
* CRUD operations for Issue Links
* CRUD operations for Issue Remote Links
* CRUD operations for Projects
* CRUD operations for Users
* CRUD operations for User Groups
* CRUD operations for Versions
* CRUD operations for Worklogs
* Myself
* Notify
* Search
* OAuth operations

[NOTE]
See a full list of operations in the link:https://hotovo.gitlab.io/mule-connectors/jira-rest-connector/javadoc/mule/jirarest-config.html[ApiDocs].


== Common Use Cases

Explain the common and less intuitive use cases and provide links to them in the bullets.
//These may include an example app that can be deployed in Mule or links to Exchange

* link:#use-case-1[Inbound Scenario]
* link:#use-case-2[Outbound Scenario]


[use-case-1]
=== Inbound Scenario
Use the connector in conjunction with a https://docs.mulesoft.com/mule-user-guide/v/3.8/poll-reference[Poll Scope] in a flow to pull data from Jira into your application. To use the connector in this capacity, you must first place a *Poll scope* element at the beginning of your flow, then place a Jira Rest connector istide it.

image::inbound.png[Inbound Scenario]

. *Poll Scope* - regularly poll data from source inside it.
. *Jira Rest Connector* - retrieves data from Jira.
. *For Each Scope* - splits retrieved collection into individual records.
. *Logger* - logs details about each record.
. *Variable Transformer* - sets watermark variable for poller.

[use-case-2]
=== Outbound Scenario
Use as an outbound connector in your flow to push data into Jira. To use the connector in this manner, simply place the connector in your flow at any point after an inbound endpoint.

image::outbound.png[Outbound Scenario]

. *HTTP Listener Endpoint* - listens for HTTP requests.
. *Transform Message* - transforms incoming message to form suitable for following Jira Rest connector.
. *Jira Rest Connector* - connects to Jira and performs selected operation using received data.


== Connector Performance

To define the pooling profile for the connector manually, access the *Pooling Profile* tab in the applicable global element for the connector.

For background information on pooling, see link:/mule-user-guide/v/3.8/tuning-performance[Tuning Performance].



== Resources

* Access the link:https://hotovo.gitlab.io/mule-connectors/jira-rest-connector/docs/release-notes.html[JIRA Rest Connector Release Notes].
